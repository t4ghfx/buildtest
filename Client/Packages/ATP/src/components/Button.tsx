import * as React from 'react';
import styled from 'styled-components';

const ButtonWrapper = styled.div`
  padding: 25px;
  background-color: #3482be;
  display: inline-block;
  border-radius: 2px;
  color: white;
  cursor: pointer;
`

export interface ButtonProps  {
  /**
   * Text to be rendered inside button
   */
  text: string;
}

/**
 * Functional Components Do Not Work With react-doggen-typescript currently.
 */
export default class Button extends React.Component<ButtonProps, {}> {

  render() {
    return (
      <ButtonWrapper>
        {this.props.text} 
      </ButtonWrapper>
    )
  }
}