import * as React from 'react';
import styled from 'styled-components';

const StyledLabel = styled.label`
  color: gray;
  display: flex;
`;

const StyledInput = styled.input`
`;

interface CheckboxProps {

  /**
   * The label text
   */
  label: string;
}

interface CheckboxState {
  /**
   * Representing if the checkbox
   * is checked
   */
  checked: boolean;
}

export default class Checkbox extends React.Component<CheckboxProps, CheckboxState> {

  constructor(props: CheckboxProps) {
    super(props);
    this.state = {
      checked: false
    }

    this._handleClick = this._handleClick.bind(this);
  }

  _handleClick() {
    this.setState({
      ...this.state, checked: !this.state.checked
    })
  }

  render() {
    return (
      <StyledLabel onClick={this._handleClick}>
          <StyledInput type='checkbox' />
          {this.props.label}
      </StyledLabel>
    )
  }
}