import * as React from 'react';
import { shallow } from 'enzyme';
import  Button from '../src/components/Button'
import toJson from 'enzyme-to-json';

describe('Button', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<Button text="Hello" />)
  });

  it('Should Match Its Snapshot', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
})