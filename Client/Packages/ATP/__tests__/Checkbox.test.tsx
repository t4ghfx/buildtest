import * as React from 'react';
import { mount } from 'enzyme';
import Checkbox from '../src/components/Checkbox/Checkbox'
import toJson from 'enzyme-to-json';

describe('Checkbox', () => {

  let wrapper;
  beforeEach( () => {
    wrapper = mount(<Checkbox label="Sample Label" />)
  });

  it('Should Match Its Snapshot', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('Should have a prop label representing the label for the checkbox', () => {
    expect(wrapper.props()).toHaveProperty('label');
  });

  it('Should render the inputted label', () => {
    expect(wrapper.html()).toMatch(new RegExp('Sample Label'));
  });

  it('Should have a state value representing if the checkbox has been checked', () => {
    expect(wrapper.state()).toHaveProperty('checked');
  });

  it('Should have a default value of its checked state value of false', () => {
    expect(wrapper.state().checked).toBeFalsy();
  });

  it('Should have a function to change the value of its checked state value', () => {
    expect(Checkbox.prototype).toHaveProperty('_handleClick')
  });

  it('Should update its value of checked upon the user clicking on it', () => {
    wrapper.simulate('click');
    expect(wrapper.state().checked).toBeTruthy();
  });

})